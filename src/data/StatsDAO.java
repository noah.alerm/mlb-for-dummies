package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StatsDAO {
    //METHODS

    /**
     * This method is used to get a specific team's total amount of runs.
     * @param id Team's ID
     * @return total runs (INT)
     */
    public static int getTotalRunsData(int id) {
        int totalRuns = 0;

        //LOCAL TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT SUM(localRuns) FROM matches WHERE localTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //RUNS
            if (result.next()) {
                totalRuns += result.getInt("sum");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        //GUEST TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT SUM(guestRuns) FROM matches WHERE guestTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //RUNS
            if (result.next()) {
                totalRuns += result.getInt("sum");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return totalRuns;
    }

    /**
     * This method is used to get a specific team's total amount of hits.
     * @param id Team's ID
     * @return total hits (INT)
     */
    public static int getTotalHitsData(int id) {
        int totalHits = 0;

        //LOCAL TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT SUM(localHits) FROM matches WHERE localTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //HITS
            if (result.next()) {
                totalHits += result.getInt("sum");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        //GUEST TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT SUM(guestHits) FROM matches WHERE guestTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //HITS
            if (result.next()) {
                totalHits += result.getInt("sum");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return totalHits;
    }

    /**
     * This method is used to get a specific team's total amount of errors.
     * @param id Team's ID
     * @return total errors (INT)
     */
    public static int getTotalErrorsData(int id) {
        int errors = 0;

        //LOCAL TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT SUM(localErrors) FROM matches WHERE localTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //ERRORS
            while (result.next()) {
                errors += result.getInt("sum");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        //GUEST TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT SUM(guestErrors) FROM matches WHERE guestTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //ERRORS
            while (result.next()) {
                errors += result.getInt("sum");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return errors;
    }

    /**
     * This method is used to count how many matches has a specific team played.
     * @param id Team's ID
     * @return matches (INT)
     */
    public static int getMatchesPlayedData(int id) {
        int matches = 0;

        //LOCAL TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT COUNT(*) FROM matches WHERE localTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //MATCHES
            if (result.next()) {
                matches += result.getInt("count");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        //GUEST TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT COUNT(*) FROM matches WHERE guestTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //MATCHES
            if (result.next()) {
                matches += result.getInt("count");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return matches;
    }

    /**
     * This method is used to count how many matches has a specific team won.
     * @param id Team's ID
     * @return wins (INT)
     */
    public static int getTotalWinsData(int id) {
        int wins = 0;

        //LOCAL TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT localRuns, guestRuns FROM matches WHERE localTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //WINS
            while (result.next()) {
                if (result.getInt("localRuns") > result.getInt("guestRuns"))
                    wins++;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        //GUEST TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT localRuns, guestRuns FROM matches WHERE guestTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //WINS
            while (result.next()) {
                if (result.getInt("localRuns") < result.getInt("guestRuns"))
                    wins++;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return wins;
    }

    /**
     * This method is used to count how many matches has a specific team lost.
     * @param id Team's ID
     * @return defeats (INT)
     */
    public static int getTotalDefeatsData(int id) {
        int defeats = 0;

        //LOCAL TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT localRuns, guestRuns FROM matches WHERE localTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //DEFEATS
            while (result.next()) {
                if (result.getInt("localRuns") < result.getInt("guestRuns"))
                    defeats++;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        //GUEST TEAM
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT localRuns, guestRuns FROM matches WHERE guestTeam = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //DEFEATS
            while (result.next()) {
                if (result.getInt("localRuns") > result.getInt("guestRuns"))
                    defeats++;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return defeats;
    }
}
