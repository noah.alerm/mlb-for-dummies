package data;

import exceptions.MatchNotFoundException;
import exceptions.MoreRunsThanHitsException;
import exceptions.TeamNotFoundException;
import exceptions.TiedResultException;
import objects.Match;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MatchDAO {
    //ATTRIBUTES
    private TeamDAO teamDAO = new TeamDAO();

    //METHODS

    /**
     * This method is used to insert a new match into the matches table in the MLB for Dummies database.
     * @param match NEW Match
     */
    public void insert(Match match) {
        //TEAM NOT FOUND EXCEPTION
        if (match.getLocalTeam() == null || match.getGuestTeam() == null) {
            try {
                throw new TeamNotFoundException(("THE MATCH HASN'T BEEN ADDED TO THE DATABASE..."));
            } catch (TeamNotFoundException e) {
                System.err.println(e.getMessage());
            }
        }
        //TEAMS FOUND
        else {
            try {
                Connection connection = Database.getInstance().getConnection();

                //QUERY
                String query = "INSERT INTO matches(localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
                PreparedStatement insertStatement = connection.prepareStatement(query);
                insertStatement.setInt(1, match.getLocalTeam().getId());
                insertStatement.setInt(2, match.getGuestTeam().getId());
                insertStatement.setInt(3, match.getLocalRuns());
                insertStatement.setInt(4, match.getGuestRuns());
                insertStatement.setInt(5, match.getLocalHits());
                insertStatement.setInt(6, match.getGuestHits());
                insertStatement.setInt(7, match.getLocalErrors());
                insertStatement.setInt(8, match.getGuestErrors());

                //INSERT
                if ((match.getLocalRuns() <= match.getLocalHits()) && (match.getGuestRuns() <= match.getGuestHits())
                        && (match.getLocalRuns() != match.getGuestRuns()))
                    insertStatement.execute();
                //MORE RUNS THAN HITS EXCEPTION
                else if ((match.getLocalRuns() > match.getLocalHits()) || (match.getGuestRuns() > match.getGuestHits())) {
                    try {
                        throw new MoreRunsThanHitsException("ERROR: It's impossible for a team to perform more runs than hits.\nTHE MATCH HASN'T BEEN ADDED TO THE DATABASE...");
                    } catch (MoreRunsThanHitsException e) {
                        System.err.println(e.getMessage());
                    }
                }
                //TIED RESULT EXCEPTION
                else if (match.getLocalRuns() == match.getGuestRuns()) {
                    try {
                        throw new TiedResultException("ERROR: The match can't end in a tie, one of the teams must win.\nTHE MATCH HASN'T BEEN ADDED TO THE DATABASE...");
                    } catch (TiedResultException e) {
                        System.err.println(e.getMessage());
                    }
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * This method is used to update an existing match from the matches table in the MLB for Dummies database.
     * @param match UPDATED Match
     */
    public void update(Match match) {
        //TEAM NOT FOUND EXCEPTION
        if (match.getLocalTeam() == null || match.getGuestTeam() == null) {
            try {
                throw new TeamNotFoundException(("THE MATCH HASN'T BEEN UPDATED..."));
            } catch (TeamNotFoundException e) {
                System.err.println(e.getMessage());
            }
        }
        //TEAMS FOUND
        else {
            try{
                Connection connection = Database.getInstance().getConnection();

                //QUERY
                String query = "UPDATE matches SET(localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) = (?, ?, ?, ?, ?, ?, ?, ?) WHERE id = ?";
                PreparedStatement insertStatement = connection.prepareStatement(query);
                insertStatement.setInt(1, match.getLocalTeam().getId());
                insertStatement.setInt(2, match.getGuestTeam().getId());
                insertStatement.setInt(3, match.getLocalRuns());
                insertStatement.setInt(4, match.getGuestRuns());
                insertStatement.setInt(5, match.getLocalHits());
                insertStatement.setInt(6, match.getGuestHits());
                insertStatement.setInt(7, match.getLocalErrors());
                insertStatement.setInt(8, match.getGuestErrors());
                insertStatement.setInt(9, match.getId());

                //UPDATE
                if ((match.getLocalRuns() <= match.getLocalHits()) && (match.getGuestRuns() <= match.getGuestHits())
                        && (match.getLocalRuns() != match.getGuestRuns()))
                    insertStatement.execute();
                //MORE RUNS THAN HITS EXCEPTION
                else if ((match.getLocalRuns() > match.getLocalHits()) || (match.getGuestRuns() > match.getGuestHits())) {
                    try {
                        throw new MoreRunsThanHitsException("ERROR: It's impossible for a team to perform more runs than hits.\nTHE MATCH HASN'T BEEN UPDATED...");
                    } catch (MoreRunsThanHitsException e) {
                        System.err.println(e.getMessage());
                    }
                }
                //TIED RESULT EXCEPTION
                else if (match.getLocalRuns() == match.getGuestRuns()) {
                    try {
                        throw new TiedResultException("ERROR: The match can't end in a tie, one of the teams must win.\nTHE MATCH HASN'T BEEN UPDATED...");
                    } catch (TiedResultException e) {
                        System.err.println(e.getMessage());
                    }
                }
            }catch (SQLException e){
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * This method is used to delete an existing match from the matches table in the MLB for Dummies database.
     * @param id Match's ID
     */
    public void delete(int id) {
        try{
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "DELETE FROM matches WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);

            //DELETE
            if (id <= list().get(list().size()-1).getId())
                insertStatement.execute();
            //MATCH NOT FOUND EXCEPTION
            else {
                try {
                    throw new MatchNotFoundException(String.format("ERROR: There's no match with ID nº%d in this database.\nTHE MATCH HASN'T BEEN DELETED...", id));
                } catch (MatchNotFoundException e) {
                    System.err.println(e.getMessage());
                }
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method is used to list all matches in the matches table in the MLB for Dummies database.
     * @return list (MATCH LIST)
     */
    public List<Match> list() {
        List<Match> list = new ArrayList<>();

        try{
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT * FROM matches ORDER BY id";
            Statement listStatement = connection.createStatement();
            ResultSet result = listStatement.executeQuery(query);

            //LIST
            while (result.next()){
                int id = result.getInt("id");
                int localTeam = result.getInt("localTeam");
                int guestTeam = result.getInt("guestTeam");
                int localRuns = result.getInt("localRuns");
                int guestRuns = result.getInt("guestRuns");
                int localHits = result.getInt("localHits");
                int guestHits = result.getInt("guestHits");
                int localErrors = result.getInt("localErrors");
                int guestErrors = result.getInt("guestErrors");

                list.add(new Match(id, teamDAO.get(localTeam), teamDAO.get(guestTeam), localRuns, guestRuns, localHits, guestHits,
                        localErrors, guestErrors));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

        return list;
    }

    /**
     * This method returns a match from the matches table in the MLB for Dummies database given its ID.
     * @param id Match's ID
     * @return Match (MATCH)
     */
    public Match get(int id) {
        Match match = null;
        try{
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT * FROM matches WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //GET
            if (result.next()) {
                int localTeam = result.getInt("localTeam");
                int guestTeam = result.getInt("guestTeam");
                int localRuns = result.getInt("localRuns");
                int guestRuns = result.getInt("guestRuns");
                int localHits = result.getInt("localHits");
                int guestHits = result.getInt("guestHits");
                int localErrors = result.getInt("localErrors");
                int guestErrors = result.getInt("guestErrors");

                match = new Match(id, teamDAO.get(localTeam), teamDAO.get(guestTeam), localRuns, guestRuns, localHits, guestHits,
                        localErrors, guestErrors);
            }
            //MATCH NOT FOUND EXCEPTION
            else {
                try {
                    throw new MatchNotFoundException(String.format("ERROR: There's no match with ID nº%d in this database.", id));
                } catch (MatchNotFoundException e) {
                    System.err.println(e.getMessage());
                }
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

        return match;
    }
}
