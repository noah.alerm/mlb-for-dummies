package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    //ATTRIBUTES
    //Database Attributes
    private static String URL = "jdbc:postgresql://rogue.db.elephantsql.com:5432/";
    private static String BD = "mvjtbybl";
    private static String USER = "mvjtbybl";
    private static String PASSWORD = "YRPBTw0Lfx_K8_QCqPADgRj-o-uRmVxx";

    //Class Attributes
    private static Database database = null;
    private Connection connection;

    //INSTANCE
    /**
     * This method creates a unique instance for the Database.
     * @return Database (DATABASE)
     */
    public static Database getInstance() {
        if (database == null)
            database = new Database();
        return database;
    }

    //CONSTRUCTOR
    /**
     * Database Constructor
     */
    public Database() {
        this.connection = null;
    }

    //GETTER
    public Connection getConnection() {
        return connection;
    }

    //DATABASE METHODS
    /**
     * This method opens a connection in the app's database.
     */
    public void connect() {
        try {
            this.connection = DriverManager.getConnection(URL+BD, USER, PASSWORD);
        } catch (Exception e) {
            System.out.println("ERROR: Couldn't access the database");
        }
    }

    /**
     * This method closes the database's connection.
     */
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("ERROR: Couldn't close the database");
        }
    }
}
