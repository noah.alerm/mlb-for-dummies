package data;

import exceptions.TeamNotFoundException;
import objects.Team;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TeamDAO {
    //METHODS

    /**
     * This method is used to insert a new team into the teams table in the MLB for Dummies database.
     * @param team NEW Team
     */
    public void insert(Team team) {
        try {
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "INSERT INTO teams(name, city) VALUES(?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, team.getName());
            insertStatement.setString(2, team.getCity());

            //INSERT
            insertStatement.execute();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method is used to update an existing team from the teams table in the MLB for Dummies database.
     * @param team UPDATED Team
     */
    public void update(Team team) {
        try{
            //TEAM FOUND
            if (team.getId() <= list().get(list().size()-1).getId()) {
                Connection connection = Database.getInstance().getConnection();

                //QUERY
                String query = "UPDATE teams SET(name, city) = (?, ?) WHERE id = ?";
                PreparedStatement insertStatement = connection.prepareStatement(query);
                insertStatement.setString(1, team.getName());
                insertStatement.setString(2, team.getCity());
                insertStatement.setInt(3, team.getId());

                //UPDATE
                insertStatement.execute();
            }
            //TEAM NOT FOUND EXCEPTION
            else {
                try {
                    throw new TeamNotFoundException(String.format("ERROR: There's no team with ID nº%d in this database.", team.getId()));
                } catch (TeamNotFoundException e) {
                    System.err.println(e.getMessage());
                }
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method is used to delete an existing team from the teams table in the MLB for Dummies database.
     * @param id Team's ID
     */
    public void delete(int id) {
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "DELETE FROM teams WHERE id = ?";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setInt(1, id);

            //DELETE
            if (id <= list().get(list().size()-1).getId())
                insertStatement.execute();
            //TEAM NOT FOUND EXCEPTION
            else {
                try {
                    throw new TeamNotFoundException(String.format("ERROR: There's no match with ID nº%d in this database.\nTHE MATCH HASN'T BEEN DELETED...", id));
                } catch (TeamNotFoundException e) {
                    System.err.println(e.getMessage());
                }
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method is used to list all teams in the teams table in the MLB for Dummies database.
     * @return list (TEAM LIST)
     */
    public List<Team> list() {
        List<Team> list = new ArrayList<>();

        try{
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT * FROM teams ORDER BY id";
            Statement listStatement = connection.createStatement();
            ResultSet result = listStatement.executeQuery(query);

            //LIST
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String city = result.getString("city");
                list.add(new Team(id, name, city));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

        return list;
    }

    /**
     * This method returns a team from the teams table in the MLB for Dummies database given its ID.
     * @param id Team's ID
     * @return Team (TEAM)
     */
    public Team get(int id) {
        Team team = null;

        try{
            Connection connection = Database.getInstance().getConnection();

            //QUERY
            String query = "SELECT * FROM teams WHERE id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();

            //GET
            if (result.next()) {
                String name = result.getString("name");
                String city = result.getString("city");

                team = new Team(id, name, city);
            }
            //TEAM NOT FOUND EXCEPTION
            else {
                try {
                    throw new TeamNotFoundException(String.format("ERROR: There's no team with ID nº%d in this database.", id));
                } catch (TeamNotFoundException e) {
                    System.err.println(e.getMessage());
                }
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

        return team;
    }
}
