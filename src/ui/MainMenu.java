package ui;

public class MainMenu extends MyScanner {
    //ATTRIBUTES
    private DataMenu dataMenu;
    private StatsMenu statsMenu;
    private StandingsMenu standingsMenu;

    //CONSTRUCTOR
    /**
     * MainMenu Constructor
     */
    public MainMenu() {
        dataMenu = new DataMenu();
        statsMenu = new StatsMenu();
        standingsMenu = new StandingsMenu();
    }

    //METHODS
    /**
     * Shows the main menu and allows to introduce an input, which leads the user to another menu.
     */
    public void showMainMenu() {
        while (true) {
            //PRINTS
            System.out.println("--------------------------");
            System.out.println("Welcome to MLB for Dummies");
            System.out.println("--------------------------");
            System.out.println("What do you want to access?");
            System.out.println("1 - Data");
            System.out.println("2 - Stats");
            System.out.println("3 - Standings");
            System.out.println("0 - EXIT");
            System.out.println("--------------------------");

            //OPTIONS
            int input = isInputValid(0, 3);
            switch (input) {
                case 0:
                    return;
                case 1:
                    dataMenu.showDataMenu();
                    break;
                case 2:
                    statsMenu.showStatsMenu();
                    break;
                case 3:
                    standingsMenu.showStandingsMenu();
                    break;
            }
        }
    }
}
