package ui;

public class StatsMenu extends MyScanner {
    //ATTRIBUTES
    private GlobalStatsMenu globalStats;
    private LocalStatsMenu localStatsMenu;

    //CONSTRUCTOR
    /**
     * StatsMenu Constructor
     */
    public StatsMenu() {
        globalStats = new GlobalStatsMenu();
        localStatsMenu = new LocalStatsMenu();
    }

    //METHODS
    /**
     * Shows the Stats Menu and leads the user to either the global stats or the local stats.
     */
    public void showStatsMenu() {
        while (true) {
            //PRINTS
            System.out.println("--------------------------");
            System.out.println("STATS MENU");
            System.out.println("--------------------------");
            System.out.println("What do you want to access?");
            System.out.println("1 - Global Stats");
            System.out.println("2 - Local Stats");
            System.out.println("0 - EXIT");
            System.out.println("--------------------------");

            //OPTIONS
            int input = isInputValid(0, 2);
            switch (input) {
                case 0:
                    return;
                case 1:
                    globalStats.showGlobalStatsMenu();
                    break;
                case 2:
                    localStatsMenu.showLocalStatsMenu();
                    break;
            }
        }
    }
}
