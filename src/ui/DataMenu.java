package ui;

import data.MatchDAO;
import data.TeamDAO;
import objects.Match;
import objects.Team;

import java.util.List;

public class DataMenu extends MyScanner {
    //ATTRIBUTES
    private static TeamDAO teamDAO;
    private MatchDAO matchDAO;

    //CONSTRUCTOR
    /**
     * DataMenu Constructor
     */
    public DataMenu() {
        teamDAO = new TeamDAO();
        matchDAO = new MatchDAO();
    }

    //METHODS
    /**
     * Shows the Data Menu and allows to introduce an input, which leads to the performance of an action.
     */
    public void showDataMenu() {
        while (true) {
            //PRINTS
            System.out.println("--------------------------");
            System.out.println("DATA MENU");
            System.out.println("--------------------------");
            System.out.println("What action do you want to perform?");
            System.out.println("1 - Add a new team");
            System.out.println("2 - Modify a team");
            System.out.println("3 - Delete a team");
            System.out.println("4 - List all teams");
            System.out.println("5 - Add a new match");
            System.out.println("6 - Modify a match");
            System.out.println("7 - Delete a match");
            System.out.println("8 - List all matches");
            System.out.println("0 - EXIT");
            System.out.println("--------------------------");

            //OPTIONS
            int input = isInputValid(0, 8);
            switch (input) {
                case 0:
                    return;
                case 1:
                    addNewTeam();
                    break;
                case 2:
                    editTeam();
                    break;
                case 3:
                    deleteTeam();
                    break;
                case 4:
                    listTeams();
                    break;
                case 5:
                    addNewMatch();
                    break;
                case 6:
                    editMatch();
                    break;
                case 7:
                    deleteMatch();
                    break;
                case 8:
                    listMatches();
                    break;
            }
        }
    }

    /**
     * Asks the info to add a new team.
     */
    public void addNewTeam() {
        System.out.println("Introduce the team's name: ");
        getScanner().nextLine();
        String name = getScanner().nextLine();
        System.out.println("Introduce the team's city: ");
        String city = getScanner().nextLine();

        teamDAO.insert(new Team(name, city));
    }

    /**
     * Edits a specific team's data.
     */
    public void editTeam() {
        listTeams();
        System.out.println("Enter the team's ID:");
        int id = getScanner().nextInt();

        getScanner().nextLine();

        System.out.println("Enter the team's NEW name:");
        String name = getScanner().nextLine();
        System.out.println("Enter the team's NEW city:");
        String city = getScanner().nextLine();

        teamDAO.update(new Team(id, name, city));
    }

    /**
     * Deletes a specific team's data.
     */
    public void deleteTeam() {
        listTeams();
        System.out.println("Enter the team's ID:");
        teamDAO.delete(getScanner().nextInt());
    }

    /**
     * Lists all the teams.
     */
    public static void listTeams() {
        System.out.println("TEAMS:");
        List<Team> teams = teamDAO.list();
        teams.forEach(t -> System.out.printf("%s→%s %s\n", "\033[0;34m", "\033[0m", t));
        System.out.println();
    }

    /**
     * Asks the info to add a new match.
     */
    public void addNewMatch() {
        listTeams();

        System.out.println("Enter the local team's ID:");
        int localTeam = getScanner().nextInt();
        System.out.println("Enter the guest team's ID: ");
        int guestTeam = getScanner().nextInt();
        System.out.println("How many runs has the local team completed?");
        int localRuns = getScanner().nextInt();
        System.out.println("How many runs has the guest team completed?");
        int guestRuns = getScanner().nextInt();
        System.out.println("How many hits has the local team accomplished?");
        int localHits = getScanner().nextInt();
        System.out.println("How many hits has the guest team accomplished?");
        int guestHits = getScanner().nextInt();
        System.out.println("How many errors has the local team made?");
        int localErrors = getScanner().nextInt();
        System.out.println("How many errors has the guest team made?");
        int guestErrors = getScanner().nextInt();

        matchDAO.insert(new Match(teamDAO.get(localTeam), teamDAO.get(guestTeam), localRuns, guestRuns, localHits, guestHits, localErrors,
                guestErrors));
    }

    /**
     * Edits a specific match's data.
     */
    public void editMatch() {
        listMatches();
        System.out.println("Enter the match's ID:");
        int id = getScanner().nextInt();

        listTeams();

        System.out.println("Enter the NEW local team's ID:");
        int localTeam = getScanner().nextInt();
        System.out.println("Enter the NEW guest team's ID:");
        int guestTeam = getScanner().nextInt();
        System.out.println("Enter the local team's NEW runs:");
        int localRuns = getScanner().nextInt();
        System.out.println("Enter the guest team's NEW runs:");
        int guestRuns = getScanner().nextInt();
        System.out.println("Enter the local team's NEW hits:");
        int localHits = getScanner().nextInt();
        System.out.println("Enter the guest team's NEW hits:");
        int guestHits = getScanner().nextInt();
        System.out.println("Enter the local team's NEW errors:");
        int localErrors = getScanner().nextInt();
        System.out.println("Enter the guest team's NEW errors:");
        int guestErrors = getScanner().nextInt();

        matchDAO.update(new Match(id, teamDAO.list().get(localTeam-1), teamDAO.list().get(guestTeam-1), localRuns, guestRuns, localHits, guestHits,
                localErrors, guestErrors));
    }

    /**
     * Deletes a specific match's data.
     */
    public void deleteMatch() {
        listMatches();
        System.out.println("Enter the match's ID:");
        matchDAO.delete(getScanner().nextInt());
    }

    /**
     * Lists all the matches.
     */
    public void listMatches() {
        System.out.println("MATCHES:");
        List<Match> matches = matchDAO.list();
        matches.forEach(m -> System.out.printf("%s→%s %s\n", "\033[0;34m", "\033[0m", m));
        System.out.println();
    }
}
