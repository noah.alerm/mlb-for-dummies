package ui;

import data.StatsDAO;
import data.TeamDAO;
import objects.Stats;
import objects.Team;

public class LocalStatsMenu extends MyScanner {
    //ATTRIBUTES
    private Stats stats;
    private TeamDAO teamDAO;

    //CONSTRUCTOR
    /**
     * GlobalStatsMenu Constructor
     */
    public LocalStatsMenu() {
        this.stats = Stats.getInstance();
        this.teamDAO = new TeamDAO();
    }

    //METHODS
    /**
     * Shows the Local Stats.
     */
    public void showLocalStatsMenu() {
        while (true) {
            //PRINTS
            System.out.println("--------------------------");
            System.out.println("LOCAL STATS MENU");
            System.out.println("--------------------------");
            System.out.println("What statistics do you want to see?");
            System.out.println("1 - Total and average runs per match");
            System.out.println("2 - Total amount of hits");
            System.out.println("3 - Total victories and defeats");
            System.out.println("4 - Winning percentage");
            System.out.println("5 - Matches played");
            System.out.println("0 - EXIT");
            System.out.println("--------------------------");

            //OPTIONS
            int input = isInputValid(0, 5);
            switch (input) {
                case 0:
                    return;
                case 1:
                    localTotalAndAvgRuns(choiceTeam());
                    break;
                case 2:
                    localTotalHits(choiceTeam());
                    break;
                case 3:
                    localTotalWinsAndLosses(choiceTeam());
                    break;
                case 4:
                    localPCT(choiceTeam());
                    break;
                case 5:
                    localPlayedGames(choiceTeam());
                    break;
            }
        }
    }

    /**
     * Allows the user to choose the team whose information they want to see.
     * @return chosen team's ID
     */
    public int choiceTeam() {
        System.out.println("Choose a team by its ID:");
        DataMenu.listTeams();
        return getScanner().nextInt();
    }

    /**
     * Shows a team's total and average runs.
     * @param id Team's ID
     */
    public void localTotalAndAvgRuns(int id) {
        Team team = teamDAO.get(id);

        System.out.printf("%s %s have achieved %d runs in %d games. This makes an average of %d runs per game.\n",
                team.getCity(), team.getName(), StatsDAO.getTotalRunsData(id), StatsDAO.getMatchesPlayedData(id),
                stats.getAverageRunsLocal(id));
    }

    /**
     * Shows a team's total hits.
     * @param id Team's ID
     */
    public void localTotalHits(int id) {
        Team team = teamDAO.get(id);

        System.out.printf("%s %s have performed a total of %d hits.\n", team.getCity(), team.getName(),
                StatsDAO.getTotalHitsData(id));
    }

    /**
     * Shows a team's total wins.
     * @param id Team's ID
     */
    public void localTotalWinsAndLosses(int id) {
        Team team = teamDAO.get(id);

        System.out.printf("%s %s have won %d times and have lost %d times.\n", team.getCity(), team.getName(),
                StatsDAO.getTotalWinsData(id), StatsDAO.getTotalDefeatsData(id));
    }

    /**
     * Shows a team's PCT.
     * @param id Team's ID
     */
    public void localPCT(int id) {
        Team team = teamDAO.get(id);

        System.out.printf("%s %s have a PCT of %s.\n", team.getCity(), team.getName(), stats.getPCTLocal(id));
    }

    /**
     * Shows a the amount of games a team has played.
     * @param id Team's ID
     */
    public void localPlayedGames(int id) {
        Team team = teamDAO.get(id);

        System.out.printf("%s %s have played a total amount of %d games.\n", team.getCity(), team.getName(),
                StatsDAO.getMatchesPlayedData(id));
    }
}
