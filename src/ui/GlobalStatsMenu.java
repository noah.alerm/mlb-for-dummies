package ui;

import data.MatchDAO;
import data.TeamDAO;
import objects.Stats;
import objects.Team;

public class GlobalStatsMenu extends MyScanner {
    //ATTRIBUTES
    private Stats stats;
    private TeamDAO teamDAO;
    private MatchDAO matchDAO;

    //CONSTRUCTOR
    /**
     * GlobalStatsMenu Constructor
     */
    public GlobalStatsMenu() {
        this.stats = Stats.getInstance();

        this.teamDAO = new TeamDAO();
        this.matchDAO = new MatchDAO();
    }

    //METHODS
    /**
     * Shows the Global Stats.
     */
    public void showGlobalStatsMenu() {
        while (true) {
            //PRINTS
            System.out.println("--------------------------");
            System.out.println("GLOBAL STATS MENU");
            System.out.println("--------------------------");
            System.out.println("What statistics do you want to see?");
            System.out.println("1 - Total runs and average runs per match");
            System.out.println("2 - Total victories and defeats per team");
            System.out.println("3 - Matches with the most and the least runs");
            System.out.println("4 - Matches with the biggest and the smallest run difference");
            System.out.println("5 - Team with the most errors");
            System.out.println("6 - Team with the biggest winning streak");
            System.out.println("7 - Winning percentage");
            System.out.println("0 - EXIT");
            System.out.println("--------------------------");

            //OPTIONS
            int input = isInputValid(0, 7);
            switch (input) {
                case 0:
                    return;
                case 1:
                    globalTotalAndAvgRuns();
                    break;
                case 2:
                    globalWinsAndDefeats();
                    break;
                case 3:
                    globalMostAndLeastRuns();
                    break;
                case 4:
                    stats.showBiggestAndSmallestRunDiffGlobal();
                    break;
                case 5:
                    stats.showMostErrorsGlobal();
                    break;
                case 6:
                    stats.showBiggestStreakGlobal();
                    break;
                case 7:
                    globalPCT();
                    break;
            }
        }
    }

    /**
     * Shows the total and the average amount of runs per match.
     */
    public void globalTotalAndAvgRuns(){
        for (int i = 0; i < teamDAO.list().size(); i++) {
            Team team = teamDAO.list().get(i);

            System.out.printf("%d. %s %s - %s\n", team.getId(), team.getCity(), team.getName(),
                    stats.showTAndAvgRunsGlobal(team.getId()));
        }
    }

    /**
     * Shows the amount of wins and defeats.
     */
    public void globalWinsAndDefeats() {
        for (int i = 0; i < teamDAO.list().size(); i++) {
            Team team = teamDAO.list().get(i);

            System.out.printf("%d. %s %s - %s\n", team.getId(), team.getCity(), team.getName(),
                    stats.showWinsAndLossesGlobal(team.getId()));
        }
    }

    /**
     * Shows the matches with the most and the least runs.
     */
    public void globalMostAndLeastRuns() {
        int mostRunsID = stats.showMostRunsGlobal();
        int leastRunsID = stats.showLeastRunsGlobal();

        System.out.println("Match with the most runs: ");
        System.out.println(matchDAO.get(mostRunsID));
        System.out.printf("Total runs: %d\n", matchDAO.get(mostRunsID).getLocalRuns() +
                matchDAO.get(mostRunsID).getGuestRuns());
        System.out.println("Match with the least runs: ");
        System.out.println(matchDAO.get(leastRunsID));
        System.out.printf("Total runs: %d\n", matchDAO.get(leastRunsID).getLocalRuns() +
                matchDAO.get(leastRunsID).getGuestRuns());
    }

    /**
     * Shows the PCTs.
     */
    public void globalPCT() {
        for (int i = 0; i < teamDAO.list().size(); i++) {
            Team team = teamDAO.list().get(i);

            System.out.printf("%d. %s %s - %s\n", team.getId(), team.getCity(), team.getName(),
                    stats.showPCTsGlobal(team.getId()));
        }
    }
}
