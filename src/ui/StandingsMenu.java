package ui;

import objects.Stats;

public class StandingsMenu extends MyScanner {
    //ATTRIBUTES
    private Stats stats;

    //CONSTRUCTOR
    /**
     * StandingsMenu Constructor
     */
    public StandingsMenu() {
        this.stats = Stats.getInstance();
    }

    //METHODS
    /**
     * Shows the Standings Menu, which allows the user to see the Standings.
     */
    public void showStandingsMenu() {
        while (true) {
            //PRINTS
            System.out.println("--------------------------");
            System.out.println("STANDINGS MENU");
            System.out.println("--------------------------");
            System.out.println("1 - See Standings");
            System.out.println("0 - EXIT");
            System.out.println("--------------------------");

            //OPTIONS
            int input = isInputValid(0, 1);
            switch (input) {
                case 0:
                    return;
                case 1:
                    stats.showStandings();
                    break;
            }
        }
    }
}
