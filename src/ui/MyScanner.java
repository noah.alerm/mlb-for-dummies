package ui;

import java.util.Locale;
import java.util.Scanner;

public class MyScanner {
    //ATTRIBUTES
    private Scanner scanner;

    //CONSTRUCTOR
    /**
     * MyScanner Constructor
     */
    public MyScanner() {
        this.scanner = new Scanner(System.in).useLocale(Locale.US);
    }

    //GETTERS
    /**
     * Allows to access the normal functions of a scanner from MyScanner.
     * @return the scanner
     */
    public Scanner getScanner() {
        return this.scanner;
    }

    //METHODS
    /**
     * Checks if the input is a valid number and prints a message if it isn't.
     * @param minValue Minimum valid number
     * @param maxValue Maximum valid number
     * @return valid number
     */
    public int isInputValid(int minValue, int maxValue) {
        int input = scanner.nextInt();

        while (input < minValue || input > maxValue) {
            System.out.printf("ERROR -- This input is invalid. Please, introduce a number between %d and %d.\n",
                    minValue, maxValue);

            input = scanner.nextInt();
        }

        return input;
    }
}
