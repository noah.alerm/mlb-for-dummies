package objects;

public class Match {
    //ATTRIBUTES
    private int id;
    private Team localTeam;
    private Team guestTeam;
    private int localRuns;
    private int guestRuns;
    private int localHits;
    private int guestHits;
    private int localErrors;
    private int guestErrors;

    //CONSTRUCTORS
    /**
     * Match Constructor
     * @param id Match's ID
     * @param localTeam Local Team's name
     * @param guestTeam Guest Team's name
     * @param localRuns Local Team's runs
     * @param guestRuns Guest Team's runs
     * @param localHits Local Team's hits
     * @param guestHits Guest Team's hits
     * @param localErrors Local Team's errors
     * @param guestErrors Guest Team's errors
     */
    public Match(int id, Team localTeam, Team guestTeam, int localRuns, int guestRuns, int localHits, int guestHits,
                 int localErrors, int guestErrors) {
        this.id = id;
        this.localTeam = localTeam;
        this.guestTeam = guestTeam;
        this.localRuns = localRuns;
        this.guestRuns = guestRuns;
        this.localHits = localHits;
        this.guestHits = guestHits;
        this.localErrors = localErrors;
        this.guestErrors = guestErrors;
    }

    /**
     * Match Constructor
     * @param localTeam Local Team's name
     * @param guestTeam Guest Team's name
     * @param localRuns Local Team's runs
     * @param guestRuns Guest Team's runs
     * @param localHits Local Team's hits
     * @param guestHits Guest Team's hits
     * @param localErrors Local Team's errors
     * @param guestErrors Guest Team's errors
     */
    public Match(Team localTeam, Team guestTeam, int localRuns, int guestRuns, int localHits, int guestHits,
                 int localErrors, int guestErrors) {
        this.localTeam = localTeam;
        this.guestTeam = guestTeam;
        this.localRuns = localRuns;
        this.guestRuns = guestRuns;
        this.localHits = localHits;
        this.guestHits = guestHits;
        this.localErrors = localErrors;
        this.guestErrors = guestErrors;
    }

    //GETTERS
    public int getId() {
        return id;
    }
    public Team getLocalTeam() {
        return localTeam;
    }
    public Team getGuestTeam() {
        return guestTeam;
    }
    public int getLocalRuns() {
        return localRuns;
    }
    public int getGuestRuns() {
        return guestRuns;
    }
    public int getLocalHits() {
        return localHits;
    }
    public int getGuestHits() {
        return guestHits;
    }
    public int getLocalErrors() {
        return localErrors;
    }
    public int getGuestErrors() {
        return guestErrors;
    }

    //ToSTRING
    /**
     * This method is used to give every match a format.
     * @return Match (STRING)
     */
    @Override
    public String toString() {
        //This variables are used to get the teams' info through their ID.
        Team local = getLocalTeam();
        Team guest = getGuestTeam();

        return String.format("%d. %s %s %d - %d %s %s, H: %d - %d, E: %d - %d",
                getId(), local.getCity(), local.getName(), getLocalRuns(), getGuestRuns(), guest.getCity(),
                guest.getName(), getLocalHits(), getGuestHits(), getLocalErrors(), getGuestErrors());
    }
}
