package objects;

public class Team {
    //ATTRIBUTES
    private int id;
    private String name;
    private String city;

    //CONSTRUCTORS
    /**
     * Team Constructor
     * @param id Team's id
     * @param name Team's name
     * @param city Team's city
     */
    public Team(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    /**
     * Team Constructor
     * @param name Team's name
     * @param city Team's city
     */
    public Team(String name, String city) {
        this.name = name;
        this.city = city;
    }

    //GETTERS
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getCity() {
        return city;
    }

    //ToSTRING
    /**
     * This method is used to give every team a format.
     * @return Team (STRING)
     */
    @Override
    public String toString() {
        return String.format("%d - %s %s", getId(), getCity(), getName());
    }
}
