package objects;

import data.MatchDAO;
import data.StatsDAO;
import data.TeamDAO;

import java.util.Locale;

public class Stats {
    //ATTRIBUTES
    private TeamDAO teamDAO;
    private MatchDAO matchDAO;
    private static Stats stats = null;

    //INSTANCE
    /**
     * Creates a unique instance for the League class.
     * @return league
     */
    public static Stats getInstance() {
        if (stats == null)
            stats = new Stats();
        return stats;
    }

    //CONSTRUCTOR
    /**
     * League Constructor
     */
    private Stats() {
        this.teamDAO = new TeamDAO();
        this.matchDAO = new MatchDAO();
    }

    //METHODS

    //STATS CALCULATIONS

    //Global Stats
    /**
     * Shows the total amount of runs and the average amount of runs per match of each team.
     * @param id Team's ID
     * @return total and average runs (each team)
     */
    public String showTAndAvgRunsGlobal(int id) {
        int totalRuns = StatsDAO.getTotalRunsData(id);
        int averageRuns = getAverageRunsLocal(id);

        return String.format("Total runs: %d, Average runs per match: %d", totalRuns, averageRuns);
    }

    /**
     * Shows the total amount of wins and losses of each team.
     * @param id Team's ID
     * @return wins and losses (each team)
     */
    public String showWinsAndLossesGlobal(int id) {
        int counterW = StatsDAO.getTotalWinsData(id);
        int counterL = StatsDAO.getTotalDefeatsData(id);

        return String.format("Wins: %d, Losses: %d", counterW, counterL);
    }

    /**
     * Shows the match with the most runs. The amount of runs is based in the addition of the local runs plus the
     * guest runs.
     * @return match with the most runs (ID)
     */
    public int showMostRunsGlobal() {
        //This ints keep track of which match has the most runs and what is that quantity.
        int mostRuns = matchDAO.list().get(0).getLocalRuns() + matchDAO.list().get(0).getGuestRuns();
        int mostRunsID = matchDAO.list().get(0).getId();

        //MATCH LOOP
        for (int i = 0; i < matchDAO.list().size(); i++) {
            int sumRuns = matchDAO.list().get(i).getLocalRuns() + matchDAO.list().get(i).getGuestRuns();

            //MOST RUNS
            if (sumRuns > mostRuns) {
                mostRuns = sumRuns;
                mostRunsID = matchDAO.list().get(i).getId();
            }
        }

        return mostRunsID;
    }

    /**
     * Shows the match with the least runs. The amount of runs is based in the addition of the local runs
     * plus the guest runs.
     * @return match with the least runs (ID)
     */
    public int showLeastRunsGlobal() {
        //This ints keep track of which match has the least runs and what is that quantity.
        int leastRuns =  matchDAO.list().get(0).getLocalRuns() + matchDAO.list().get(0).getGuestRuns();
        int leastRunsID = matchDAO.list().get(0).getId();

        //MATCH LOOP
        for (int i = 0; i < matchDAO.list().size(); i++) {
            int sumRuns =  matchDAO.list().get(i).getLocalRuns() + matchDAO.list().get(i).getGuestRuns();

            //LEAST RUNS
            if (sumRuns < leastRuns) {
                leastRuns = sumRuns;
                leastRunsID = matchDAO.list().get(i).getId();
            }
        }

        return leastRunsID;
    }

    /**
     * Shows the matches with the biggest and the smallest run difference.
     */
    public void showBiggestAndSmallestRunDiffGlobal() {
        //These ints are used to initiate the variable smallRunDifference.
        int initLocalRuns = matchDAO.list().get(0).getLocalRuns();
        int initGuestRuns = matchDAO.list().get(0).getGuestRuns();

        //Keeps track of the biggest and the smallest run difference and the matches where they happened
        int biggestRunDifference = initLocalRuns - initGuestRuns;
        int biggestID = 1;

        int smallRunDifference = 0;
        //The if is used in order to prevent the result being a negative number.
        if (initLocalRuns > initGuestRuns)
            smallRunDifference = initLocalRuns - initGuestRuns;
        else if (initLocalRuns < initGuestRuns)
            smallRunDifference = initGuestRuns - initLocalRuns;
        int smallestID = 1;

        //MATCH LOOP
        for (int i = 1; i < matchDAO.list().size(); i++) {
            int localRuns = matchDAO.list().get(i).getLocalRuns();
            int guestRuns = matchDAO.list().get(i).getGuestRuns();
            int runDifference = 0;

            //Calculates this match's run difference and prevents the result from being a negative number.
            if (localRuns > guestRuns)
                runDifference = localRuns - guestRuns;
            else if (localRuns < guestRuns)
                runDifference = guestRuns -localRuns;

            //BIGGEST
            if (runDifference > biggestRunDifference) {
                biggestRunDifference = runDifference;
                biggestID = matchDAO.list().get(i).getId();
            }
            //SMALLEST
            if (runDifference < smallRunDifference) {
                smallRunDifference = runDifference;
                smallestID = matchDAO.list().get(i).getId();
            }
        }

        //OUTPUT
        System.out.println("Match with the biggest run difference: ");
        System.out.println(matchDAO.get(biggestID));
        System.out.printf("Run difference: %d\n", biggestRunDifference);
        System.out.println("Match with the smallest run difference: ");
        System.out.println(matchDAO.get(smallestID));
        System.out.printf("Run difference: %d\n", smallRunDifference);
    }

    /**
     * Shows the team that has made the most errors.
     */
    public void showMostErrorsGlobal() {
        //This ints are used to keep track of the biggest amount of errors and the team that made them.
        int mostErrors = 0;
        int mostErrorsID = 0;

        //TEAM LOOP
        for (int i = 1; i < teamDAO.list().size(); i++) {
            int errorCounter = StatsDAO.getTotalErrorsData(teamDAO.list().get(i).getId());

            //MOST ERRORS
            if (errorCounter > mostErrors) {
                mostErrors = errorCounter;
                mostErrorsID = teamDAO.list().get(i).getId();
            }
        }

        //OUTPUT
        System.out.println("The team that has committed the most errors is:");
        System.out.println(teamDAO.get(mostErrorsID));
        System.out.printf("Total errors: %d\n", mostErrors);
    }

    /**
     * Shows the team that has the biggest winning streak.
     */
    public void showBiggestStreakGlobal() {
        //This ints keep track of the team with the biggest streak.
        int biggestStreak = Integer.parseInt(showSTRKStandings(0).replaceAll("[^0-9]", ""));
        int biggestStreakID = 1;

        //TEAM LOOP
        for (int i = 0; i < teamDAO.list().size(); i++) {
            int winStreak;
            boolean isWin = false;

            //This calculates the team's streak and checks if it's a victory.
            String streak = showSTRKStandings(i);
            if (streak.replaceAll("[^a-zA-Z].*", "").equals("W"))
                isWin = true;
            winStreak = Integer.parseInt(streak.replaceAll("[^0-9]", ""));

            //BIGGEST WIN
            //Checks if this team's streak is the biggest.
            if (winStreak > biggestStreak && isWin) {
                biggestStreak = winStreak;
                biggestStreakID = teamDAO.list().get(i).getId();
            }
        }

        //OUTPUT
        System.out.println("The team with the biggest winning streak is:");
        System.out.println(teamDAO.get(biggestStreakID));
        System.out.printf("Current streak: W%d\n", biggestStreak);
    }

    /**
     * Shows each team's PCT (winning percentage).
     * @param id Team's ID
     * @return PCTs
     */
    public String showPCTsGlobal(int id) {
        String pct = getPCTLocal(id);

        return String.format("PCT: %s", pct);
    }

    //Local Stats
    /**
     * Calculates a specific team's average amount of runs per match.
     * @param id Team's ID
     * @return average runs
     */
    public int getAverageRunsLocal(int id) {
        //When a team hasn't played any matches, there's an error, as it causes a division by zero.
        if (StatsDAO.getMatchesPlayedData(id) != 0)
            return StatsDAO.getTotalRunsData(id)/ StatsDAO.getMatchesPlayedData(id);
        else
            return 0;
    }

    /**
     * Calculates and gives a format to a team's PCT (Winning Percentage).
     * @param id Team's ID
     * @return PCT
     */
    public String getPCTLocal(int id) {
        Locale.setDefault(Locale.US);

        //Calculates and prints the winning percentage (PCT).
        double pct = (double) StatsDAO.getTotalWinsData(id) / StatsDAO.getMatchesPlayedData(id);
        return String.valueOf(String.format("%.03f", pct));
    }

    //STANDINGS
    /**
     * Shows the team's current streak.
     * @param id Team's ID
     * @return team's streak
     */
    public String showSTRKStandings(int id) {
        //These variables keep track of the team's streak.
        int winStreak = 0;
        int lossStreak = 0;
        boolean isWin = false;

        //MATCH LOOP
        for (int i = 0; i < matchDAO.list().size(); i++) {
            Match currentMatch = matchDAO.list().get(i);

            //Current team = local team
            if (currentMatch.getLocalTeam().getId() == id && (currentMatch.getLocalRuns() >
                    currentMatch.getGuestRuns())) {
                isWin = true;
                winStreak++;
                lossStreak = 0;
            }
            else if (currentMatch.getLocalTeam().getId() == id && (currentMatch.getLocalRuns() <
                    currentMatch.getGuestRuns())) {
                isWin = false;
                winStreak = 0;
                lossStreak++;
            }
            //Current team = guest team
            if (currentMatch.getGuestTeam().getId() == id && (currentMatch.getLocalRuns() <
                    currentMatch.getGuestRuns())) {
                isWin = true;
                winStreak++;
                lossStreak = 0;
            }
            else if (currentMatch.getGuestTeam().getId() == id && (currentMatch.getLocalRuns() >
                    currentMatch.getGuestRuns())) {
                isWin = false;
                winStreak = 0;
                lossStreak++;
            }
        }

        //OUTPUT
        if (isWin)
            return String.format("W%d", winStreak);
        else
            return String.format("L%d", lossStreak);
    }

    /**
     * Shows the score of a team's best match (Local).
     * @param id Team's ID
     * @return score
     */
    public String showHomeStandings(int id) {
        int biggestRunDifference = 0;
        int localRuns = 0;
        int guestRuns = 0;

        //MATCH LOOP
        //This for checks the biggest run difference performed by the local team.
        for (int i = 0; i < matchDAO.list().size(); i++) {
            Match m = matchDAO.list().get(i);
            int runDiff = 0;

            if (m.getLocalTeam().getId() == id) {
                runDiff = m.getLocalRuns() - m.getGuestRuns();
            }

            //BIGGEST
            if (runDiff > biggestRunDifference) {
                biggestRunDifference = runDiff;
                localRuns = m.getLocalRuns();
                guestRuns = m.getGuestRuns();
            }
        }

        String score = String.format("%d-%d", localRuns, guestRuns);

        //OUTPUT
        //If the result is 0-0, it returns "null" instead.
        if (score.equals("0-0"))
            return null;
        else
            return score;
    }

    /**
     * Shows the score of a team's best match (Away).
     * @param id Team's ID
     * @return score
     */
    public String showAwayStandings(int id) {
        int biggestRunDifference = 0;
        int localRuns = 0;
        int guestRuns = 0;

        //MATCH LOOP
        //This for checks the biggest run difference performed by the guest team.
        for (int i = 0; i < matchDAO.list().size(); i++) {
            Match m = matchDAO.list().get(i);
            int runDiff = 0;

            if (m.getGuestTeam().getId() == id) {
                runDiff = m.getGuestRuns() - m.getLocalRuns();
            }

            //BIGGEST
            if (runDiff > biggestRunDifference) {
                biggestRunDifference = runDiff;
                localRuns = m.getLocalRuns();
                guestRuns = m.getGuestRuns();
            }
        }

        String score = String.format("%d-%d", localRuns, guestRuns);

        //OUTPUT
        //If the result is 0-0, it returns "null" instead.
        if (score.equals("0-0"))
            return null;
        else
            return score;
    }

    /**
     * Shows this league's Standings.
     */
    public void showStandings() {
        System.out.printf("%2s - %8s - %2s - %2s - %5s - %4s - %3s - %2s - %5s - %5s\n", "#", "Team", "W", "L", "PCT", "STRK", "RS", "E", "HOME", "AWAY");
        for (int i = 0; i < teamDAO.list().size(); i++) {
            System.out.printf("%2d - ", teamDAO.list().get(i).getId());
            System.out.printf("%8s - ", teamDAO.list().get(i).getName());
            System.out.printf("%2d - ", StatsDAO.getTotalWinsData(teamDAO.list().get(i).getId()));
            System.out.printf("%2d - ", StatsDAO.getTotalDefeatsData(teamDAO.list().get(i).getId()));
            System.out.printf("%5s - ", getPCTLocal(teamDAO.list().get(i).getId()));
            System.out.printf("%4s - ", showSTRKStandings(teamDAO.list().get(i).getId()));
            System.out.printf("%3d - ", StatsDAO.getTotalRunsData(teamDAO.list().get(i).getId()));
            System.out.printf("%2d - ", StatsDAO.getTotalErrorsData(teamDAO.list().get(i).getId()));
            System.out.printf("%5s - ", showHomeStandings(teamDAO.list().get(i).getId()));
            System.out.printf("%5s\n", showAwayStandings(teamDAO.list().get(i).getId()));
        }
        System.out.println();

        System.out.println("*: If HOME or AWAY is null, it means that that specific team hasn't won any match at home " +
                "or away respectively.");
    }
}
