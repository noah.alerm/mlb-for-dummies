package exceptions;

public class MatchNotFoundException extends Exception {
    //CONSTRUCTOR
    /**
     * MatchNotFoundException Constructor
     * @param message Exception Message
     */
    public MatchNotFoundException(String message) {
        super(message);
    }
}
