package exceptions;

public class MoreRunsThanHitsException extends Exception {
    //CONSTRUCTOR
    /**
     * MoreRunsThanHitsException Constructor
     * @param message Exception Message
     */
    public MoreRunsThanHitsException(String message) {
        super(message);
    }
}
