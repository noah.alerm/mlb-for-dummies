package exceptions;

public class TeamNotFoundException extends Exception {
    //CONSTRUCTOR
    /**
     * TeamNotFoundException Constructor
     * @param message Exception Message
     */
    public TeamNotFoundException(String message) {
        super(message);
    }
}
