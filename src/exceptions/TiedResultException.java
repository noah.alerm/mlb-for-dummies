package exceptions;

public class TiedResultException extends Exception {
    //CONSTRUCTOR
    /**
     * TiedResultException Constructor
     * @param message Exception Message
     */
    public TiedResultException(String message) {
        super(message);
    }
}
