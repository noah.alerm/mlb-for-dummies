import data.Database;
import ui.MainMenu;

public class Main {
    //MAIN
    public static void main(String[] args) {
        Database.getInstance().connect();

        MainMenu menu = new MainMenu();
        menu.showMainMenu();

        Database.getInstance().close();
    }
}
