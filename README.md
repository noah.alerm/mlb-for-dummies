# Projecte UF6: Introducció a la persistència en BD

Aquest projecte consisteix en la creació d'una App amb l'objectiu de gestionar informació a través d'una base de 
dades. En el meu cas l'aplicació guarda dades sobre la MLB (Major League Baseball). Les dades introduïdes
a l'aplicació s'emmagatzemen en una base de dades gestionada en la classe `Database`, dins el directori `data` del 
projecte. A més, les queries de cada taula es guarden en les classes `TeamDAO` i `MatchDAO`. També hi ha una classe 
anomenada `StatsDAO` on es guarden diverses queries que obtenen dades de les estadístiques directament des de la base de
dades.

Tots els equips es guarden a la taula `teams` de la base de dades, mentre els partits s'emmagatzemen a la taula 
`matches`. En l'entrega d'aquest projecte, dins el Classroom, hi ha un fitxer SQL amb el codi de les dues taules, a més 
de diversos exemples de dades, tant d'equips com de partits. Tot i que he afegit 28 partits en aquest fitxer, és 
recomanable no utilitzar-los tots, ja que l'aplicació hauria de processar una gran quantitat de dades i, com que hauria 
d'obtenir molta informació de la base de dades, el rendiment d'aquesta es veuria dràsticament reduït.

## Menú principal

Des del menú principal podem accedir a la resta de menús:

```` 
--------------------------
Welcome to MLB for Dummies
--------------------------
What do you want to access?
1 - Data
2 - Stats
3 - Standings
0 - EXIT
--------------------------
```` 

## Menú de dades

Des del menú de dades podem manipular la informació, és a dir, podem afegir, modificar, esborrar i llistar qualsevol
tipus de dades.

````
--------------------------
DATA MENU
--------------------------
What action do you want to perform?
1 - Add a new team
2 - Modify a team
3 - Delete a team
4 - List all teams
5 - Add a new match
6 - Modify a match
7 - Delete a match
8 - List all matches
0 - EXIT
--------------------------
````

Les dades que es guarden en l'app són:

* Equips:
    * ID
    * Nom
    * Ciutat
* Partits:
    * ID
    * Equip local
    * Equip visitant
    * Runs locals
    * Runs del visitant
    * Hits locals
    * Hits del visitant
    * Errors locals
    * Errors del visitant

## Menú d'estadístiques (Stats)

Des del menú d'estadístiques podem accedir a les estadístiques globals o locals.

````
--------------------------
STATS MENU
--------------------------
What do you want to access?
1 - Global Stats
2 - Local Stats
0 - EXIT
--------------------------
````

Les estadístiques calculades per l'aplicació són:

* Estadístiques globals:
    * Nombre total de runs i mitja de runs per partit
      de cada equip
    * Nombre total de victòries i derrotes per equip
    * Partit amb més / menys runs
    * Partit amb més / menys diferència de runs
    * Equip amb més errors
    * Equip amb una ratxa major
    * Percentatges de victòria
* Estadístiques locals:
    * Nombre total de runs i mitja de runs
    * Nombre total de hits
    * Nombre total de victòries i derrotes
    * Percentatge de victòria
    * Quantitat de partits jugats

## Menú d'estadístiques globals

Des del menú d'estadístiques globals podem accedir a informació tant dels equips com dels partits.

````
--------------------------
GLOBAL STATS MENU
--------------------------
What statistics do you want to see?
1 - Total runs and average runs per match
2 - Total victories and defeats per team
3 - Matches with the most and the least runs
4 - Matches with the biggest and the smallest run difference
5 - Team with the most errors
6 - Team with the biggest winning streak
7 - Winning percentage
0 - EXIT
--------------------------
````

Depenent de la quantitat d'equips i partits emmagatzemats en la base de dades, algunes d'aquestes estadístiques poden 
tardar una estona en carrgegar-se. 

### Estadístiques de càrrega lenta:
````
3 - Matches with the most and the least runs --> Temps de càrrega aproximat: +1 minut / 10 partits 
4 - Matches with the biggest and the smallest run difference --> Temps de càrrega aproximat: 35 segons / 10 partits
6 - Team with the biggest winning streak --> Temps de càrrega aproximat: +1 minut / 10 partits
````

## Menú d'estadístiques locals

Des d'aquest menú podem visualitzar informació sobre un equip en concret.

````
--------------------------
LOCAL STATS MENU
--------------------------
What statistics do you want to see?
1 - Total and average runs per match
2 - Total amount of hits
3 - Total victories and defeats
4 - Winning percentage
5 - Matches played
0 - EXIT
--------------------------
````

## Menú de classificacions (Standings)

En el menú de classificacions tenim l'opció de veure la taula de classificacions. Tot i que no està ordenada, segons 
les victòries, és molt útil per veure tota la informació general de tots els partits.

````
--------------------------
STANDINGS MENU
--------------------------
1 - See Standings
0 - EXIT
--------------------------
````

La classificació mostra la següent informació dels equips:

* ID `#`
* Equip `Team`
* Victòries `W`
* Derrotes `L`
* Percentatge de victòria `PCT`
* Ratxa `STRK`
* Runs `RS`
* Errors `E`
* Rècord local (partit amb més diferència de runs) `HOME`
* Rècord de visitant (partit amb més diferència de runs) `AWAY`

Depenent de la quantitat d'equips i partits emmagatzemats en la base de dades, algunes parts de la classificació poden
tardar una estona en carrgegar-se.

### Parts de càrrega lenta:
````
STRK --> Temps de càrrega aproximat: 26 segons / 10 partits 
HOME --> Temps de càrrega aproximat: 25 segons / 10 partits
AWAY --> Temps de càrrega aproximat: 25 segons / 10 partits
````
