------- TABLES -------

-- TEAMS
CREATE TABLE teams (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL
);

-- MATCHES
CREATE TABLE matches (
    id SERIAL PRIMARY KEY,
    localTeam SERIAL NOT NULL,
    guestTeam SERIAL NOT NULL,
    localRuns NUMERIC,
    guestRuns NUMERIC,
    localHits NUMERIC,
    guestHits NUMERIC,
    localErrors NUMERIC,
    guestErrors NUMERIC,
    CONSTRAINT FK_local_team FOREIGN KEY (localTeam) REFERENCES teams (id) ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT FK_guest_team FOREIGN KEY (guestTeam) REFERENCES teams (id) ON DELETE SET NULL ON UPDATE CASCADE
);


------- INSERTS -------

-- TEAMS
/*1*/INSERT INTO teams (name, city) VALUES ('Yankees', 'New York');
/*2*/INSERT INTO teams (name, city) VALUES ('Astros', 'Houston');
/*3*/INSERT INTO teams (name, city) VALUES ('Red Sox', 'Boston');
/*4*/INSERT INTO teams (name, city) VALUES ('Blue Jays', 'Toronto');
/*5*/INSERT INTO teams (name, city) VALUES ('Angels', 'Los Angeles');
/*6*/INSERT INTO teams (name, city) VALUES ('Orioles', 'Baltimore');
/*7*/INSERT INTO teams (name, city) VALUES ('Rangers', 'Texas');
/*8*/INSERT INTO teams (name, city) VALUES ('White Sox', 'Chicago');

-- MATCHES
/*1*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (1, 2, 5, 2, 12, 8, 0, 1);
/*2*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (4, 1, 3, 2, 5, 6, 1, 2);
/*3*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (1, 3, 2, 3, 5, 6, 1, 2);
/*4*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (5, 1, 2, 3, 6, 13, 1, 0);
/*5*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (1, 6, 7, 2, 18, 5, 2, 1);
/*6*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (8, 1, 0, 12, 5, 21, 4, 2);
/*7*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (1, 7, 2, 1, 8, 12, 2, 3);
/*8*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (2, 4, 3, 7, 8, 14, 1, 0);
/*9*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (3, 2, 5, 4, 10, 6, 0, 0);
/*10*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (2, 5, 1, 3, 7, 9, 1, 0);
/*11*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (6, 2, 2, 3, 10, 8, 2, 1);
/*12*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (2, 8, 1, 4, 5, 9, 0, 1);
/*13*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (7, 2, 1, 2, 12, 10, 0, 1);
/*14*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (3, 4, 12, 8, 15, 10, 1, 2);
/*15*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (5, 3, 3, 2, 12, 14, 0, 0);
/*16*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (3, 6, 7, 4, 12, 7, 1, 1);
/*17*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (8, 3, 0, 6, 3, 14, 3, 1);
/*18*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (7, 3, 3, 2, 5, 4, 1, 2);
/*19*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (4, 5, 1, 4, 8, 16, 1, 2);
/*20*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (6, 4, 4, 2, 10, 15, 1, 2);
/*21*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (4, 8, 7, 1, 16, 5, 0, 3);
/*22*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (7, 4, 4, 2, 8, 6, 0, 0);
/*23*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (6, 5, 2, 4, 6, 16, 3, 0);
/*24*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (8, 6, 3, 2, 5, 8, 2, 1);
/*25*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (6, 7, 2, 1, 7, 4, 0, 1);
/*26*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (5, 8, 10, 2, 19, 6, 0, 2);
/*27*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (7, 5, 5, 4, 9, 8, 0, 0);
/*28*/INSERT INTO matches (localTeam, guestTeam, localRuns, guestRuns, localHits, guestHits, localErrors, guestErrors) 
VALUES (7, 8, 6, 5, 12, 7, 2, 3);
